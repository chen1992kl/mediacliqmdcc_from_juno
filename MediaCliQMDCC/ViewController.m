//
//  ViewController.m
//  MediaCliQMDCC
//
//  Created by Chen Kwan Lok on 2018/4/26.
//  Copyright © 2018年 Chen Kwan Lok. All rights reserved.
//

#import "ViewController.h"
#import "HDConstant.h"
#import "PBHit.h"
#import "PBHitService.h"
#import "UIImageView+WebCache.h"
//#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    NSArray     *hits;
     UICollectionView *_collectionView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-50) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [_collectionView setBackgroundColor:[UIColor lightGrayColor]];
    
    [self.view addSubview:_collectionView];
    
    self.searchBar.delegate = self;
    [self reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData
{
    [[[PBHitService sharedInstance] getPBHits]
     subscribeNext:^(NSArray *x) {
         hits = x;
         NSLog(@"this = %@",[x description]);
         for (int i = 0 ; i < [hits count]; i ++) {
             
             PBHit *item = hits[i];
             
//             NSLog(@"item.previewURL i = %d , url = %@",i,item.previewURL.absoluteString);
         }
         [_collectionView reloadData];
     }
     error:^(NSError *error) {
         NSLog(@"error : %@",error);
     }];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSLog(@"search bar = %@",searchBar.text);
    
    [[NSUserDefaults standardUserDefaults] setObject:searchBar.text forKey:@"keyword"];
    [self reloadData];
    // Do the search...
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return hits.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [cell.layer setBorderWidth:1];
    [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    UIImageView *recipeImageView =  [[UIImageView alloc] initWithFrame:CGRectMake(0,0, (self.view.frame.size.width-20) /2, 100)];


    PBHit *item = hits[indexPath.row];
    [recipeImageView sd_setImageWithURL:item.previewURL];
    [cell addSubview:recipeImageView];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width-20) /2, 100);
}

@end
