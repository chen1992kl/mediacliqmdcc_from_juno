//
//  AppDelegate.h
//  MediaCliQMDCC
//
//  Created by Chen Kwan Lok on 2018/4/26.
//  Copyright © 2018年 Chen Kwan Lok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

